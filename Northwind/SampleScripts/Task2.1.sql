﻿--1
select
sum (UnitPrice * Quantity * (1 - Discount)) as Total
from [Order Details]

--2
select
count (*) - count(ShippedDate) as 'Not Shipped Count'
from Orders

--3
select
count (distinct CustomerId) as 'Number of Customers'
from Orders