﻿--1
select 
OrderId, 
ShippedDate, 
ShipVia
from Orders
where ShippedDate >= '1998-05-06'
and ShipVia >= 2

--2
select
OrderId,
--'Not Shipped' as ShippedDate
ShippedDate = case when ShippedDate is null then 'Not Shipped' end
from Orders
where ShippedDate is null

--3
select 
OrderId as 'Order Number', 
'Shipped Date' = 
	case 
		when ShippedDate is null then 'Not Shipped' 
		else CONVERT(VARCHAR(19),ShippedDate) 
	end
from Orders
where ShippedDate >= '1998-05-06'
or ShippedDate is null