﻿--1
select distinct
(select CompanyName from Suppliers where SupplierID = prod.SupplierID) as CompanyName
from Products as prod
where UnitsInStock = 0

--2
select
Employee
from (select 
(select CONCAT(FirstName, ' ', LastName) from Employees where EmployeeID = ord.EmployeeID) as Employee,
OrderID
from Orders as ord) as custom
group by Employee
having count(OrderId) > 150

--3
select
ContactName
from Customers as cust
where not exists(select * from Orders where CustomerID = cust.CustomerID)