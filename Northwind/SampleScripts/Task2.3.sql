﻿--1
select
CONCAT(emp.FirstName, ' ', emp.LastName) as Employee,
reg.RegionDescription 
from Employees as emp
join Territories as terr 
on emp.City = terr.TerritoryDescription
join Region as reg
on terr.RegionID = reg.RegionID
where reg.RegionDescription = 'western'

--2
select
ContactName as Customer,
count(OrderId) as Amount
from Customers as cust
full join Orders as ord
on cust.CustomerID = ord.CustomerID
group by ContactName
order by Amount