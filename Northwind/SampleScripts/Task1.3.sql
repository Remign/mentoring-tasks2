﻿--1
select distinct
OrderId
from [Order Details]
where Quantity between 3 and 10

--2
select
CustomerId,
Country
from Customers
where LEFT(Country, 1) between 'b' and 'g' -- LEFT(Country, 1) = SUBSTRING(Country, 1, 1) 
order by Country

--3
select
CustomerId,
Country
from Customers
where Country LIKE '[b-g]%'
order by Country