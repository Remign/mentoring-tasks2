﻿--1
select
year(OrderDate),
count(OrderID) as Total
from Orders
group by year(OrderDate)

--2
select 
(select CONCAT(FirstName, ' ', LastName) from Employees where EmployeeID = o.EmployeeID) as Seller,
count(OrderID) as Amount
from Orders as o
where EmployeeID is not null
group by EmployeeID
order by Amount desc

--3
select
(select CONCAT(FirstName, ' ', LastName) from Employees where EmployeeID = o.EmployeeID) as Seller,
(select ContactName from Customers where CustomerID = o.CustomerID) as Customer,
count(OrderID) as Amount
from Orders as o
where year(OrderDate) = '1998'
group by EmployeeID, CustomerID
order by Amount desc

--4
SELECT c.ContactName AS Person, 'Customer' AS Type,c.City AS City
FROM Customers AS c
WHERE EXISTS (
              SELECT e.City 
              FROM Employees AS e
              WHERE e.City=c.City
              )
UNION ALL
SELECT e.FirstName+' '+e.LastName AS Person, 'Seller' AS Type,e.City AS City
FROM Employees AS e
WHERE EXISTS (
              SELECT c.City 
              FROM Customers AS c
              WHERE e.City=c.City
              )

--5
select
ContactName,
City
from Customers
group by City, ContactName

--6
select 
CONCAT(FirstName, ' ', LastName) as Employee,
(select CONCAT(h.FirstName, ' ', h.LastName) from Employees as h where h.EmployeeID = main.ReportsTo) as Head
from Employees as main