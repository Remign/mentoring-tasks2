﻿CREATE TABLE [dbo].[CreditСard] (
    [Id]          INT          NOT NULL,
    [CardNumber]  VARCHAR (16) NOT NULL,
    [ExprireDate] DATETIME     NOT NULL,
    [EmployeeID]  INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Card_Employees] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employees] ([EmployeeID])
);

