﻿using System;
using System.Data.Common;

namespace NorthwindDAL
{
  public static class Helpers
  {
    public static string SafeGetString(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetString(colIndex) : null;
    }

    public static int? SafeGetInt32(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetInt32(colIndex) : (int?) null;
    }

    public static short? SafeGetInt16(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetInt16(colIndex) : (short?)null;
    }

    public static decimal? SafeGetDecimal(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetDecimal(colIndex) : (decimal?)null;
    }

    public static double? SafeGetDouble(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetDouble(colIndex) : (double?)null;
    }

    public static double? SafeGetDobule(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetDouble(colIndex) : (double?)null;
    }

    public static float? SafeGetFloat(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetFloat(colIndex) : (float?)null;
    }

    public static DateTime? SafeGetDateTime(this DbDataReader reader, int colIndex)
    {
      return !reader.IsDBNull(colIndex) ? reader.GetDateTime(colIndex) : (DateTime?) null;
    }

    public static DbParameter CreateParameter(this DbCommand command, string parameterName, object value)
    {
      var parameter = command.CreateParameter();
      parameter.ParameterName = parameterName;
      parameter.Value = value ?? DBNull.Value;

      return parameter;
    }

  }
}