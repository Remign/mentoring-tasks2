﻿using System;
using System.Collections.Generic;
using NorthwindDAL.Models;

namespace NorthwindDAL.DALInterfaces
{
  public interface IOrderRepository
  {
    IEnumerable<Order> GetOrders();
    DetailedOrder GetDetailedOrderById(int id);
    int CreateOrder(Order newOrder);
    void UpdateOrder(Order updatedOrder);
    void DeleteOrder(int id);
    void SetOrderDate(int id, DateTime orderDate);
    void SetShippedDate(int id, DateTime shippedDate);
    IEnumerable<OrderHist> CustOrderHist(string customerId);
    IEnumerable<OrdersDetailSp> CustOrdersDetail(int id);
  }
}