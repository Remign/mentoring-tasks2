﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using NorthwindDAL.DALInterfaces;
using NorthwindDAL.Models;

namespace NorthwindDAL.Repositories
{
  public class OrderRepository : IOrderRepository
  {
    private readonly DbProviderFactory _providerFactory;
    private readonly string _connectionString;

    public OrderRepository(string provider, string connectionString)
    {
      _connectionString = connectionString;
      _providerFactory = DbProviderFactories.GetFactory(provider);
    }

    public IEnumerable<Order> GetOrders()
    {
      var resultOrders = new List<Order>();

      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText = "select * from dbo.Orders";
          command.CommandType = CommandType.Text;

          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              var order = this.GetOrder(reader);

              resultOrders.Add(order);
            }
          }
        }
      }

      return resultOrders;
    }

    public DetailedOrder GetDetailedOrderById(int id)
    {
      DetailedOrder detailedOrder;

      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "select * from dbo.Orders where OrderID = 10248 select OrderId, od.ProductId, od.UnitPrice, Quantity, Discount, ProductName from dbo.[Order Details] as od join dbo.Products as pr on od.ProductID = pr.ProductID where OrderID = @id";
          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@id", id));

          using (var reader = command.ExecuteReader())
          {
            reader.Read();
            var order = this.GetOrder(reader);

            detailedOrder = new DetailedOrder(order);

            reader.NextResult();

            while (reader.Read())
            {
              var orderDetail = this.GetOrderDetail(reader);

              detailedOrder.OrderDetails.Add(orderDetail);
            }

          }
        }
      }

      return detailedOrder;
    }

    public int CreateOrder(Order newOrder)
    {
      int result;

      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "insert into dbo.Orders (CustomerID, EmployeeID, OrderDate, RequiredDate, ShippedDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry) values (@CustomerID, @EmployeeID, @OrderDate, @RequiredDate, @ShippedDate, @ShipVia, @Freight, @ShipName, @ShipAddress, @ShipCity, @ShipRegion, @ShipPostalCode, @ShipCountry); SELECT CAST(scope_identity() AS int)";
            
          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@CustomerID", newOrder.CustomerId));
          command.Parameters.Add(command.CreateParameter("@EmployeeID", newOrder.EmployeeId));
          command.Parameters.Add(command.CreateParameter("@OrderDate", newOrder.OrderDate));
          command.Parameters.Add(command.CreateParameter("@RequiredDate", newOrder.RequiredDate));
          command.Parameters.Add(command.CreateParameter("@ShippedDate", newOrder.ShippedDate));
          command.Parameters.Add(command.CreateParameter("@ShipVia", newOrder.ShipVia));
          command.Parameters.Add(command.CreateParameter("@Freight", newOrder.Freight));
          command.Parameters.Add(command.CreateParameter("@ShipName", newOrder.ShipName));
          command.Parameters.Add(command.CreateParameter("@ShipAddress", newOrder.ShipAddress));
          command.Parameters.Add(command.CreateParameter("@ShipCity", newOrder.ShipCity));
          command.Parameters.Add(command.CreateParameter("@ShipRegion", newOrder.ShipRegion));
          command.Parameters.Add(command.CreateParameter("@ShipPostalCode", newOrder.ShipPostalCode));
          command.Parameters.Add(command.CreateParameter("@ShipCountry", newOrder.ShipCountry));

          result = (int)command.ExecuteScalar();
        }
      }

      return result;
    }

    public void UpdateOrder(Order updatedOrder)
    {
      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "update dbo.Orders set CustomerId = @CustomerId, EmployeeID = @EmployeeID, RequiredDate = @RequiredDate, ShipVia = @ShipVia, Freight = @Freight, ShipName = @ShipName, ShipAddress = @ShipAddress, ShipCity = @ShipCity, ShipRegion = @ShipRegion, ShipPostalCode = @ShipPostalCode, ShipCountry = @ShipCountry where OrderId = @OrderId and OrderDate is null and ShippedDate is null";

          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@OrderId", updatedOrder.OrderId));
          command.Parameters.Add(command.CreateParameter("@CustomerID", updatedOrder.CustomerId));
          command.Parameters.Add(command.CreateParameter("@EmployeeID", updatedOrder.EmployeeId));
          command.Parameters.Add(command.CreateParameter("@RequiredDate", updatedOrder.RequiredDate));
          command.Parameters.Add(command.CreateParameter("@ShipVia", updatedOrder.ShipVia));
          command.Parameters.Add(command.CreateParameter("@Freight", updatedOrder.Freight));
          command.Parameters.Add(command.CreateParameter("@ShipName", updatedOrder.ShipName));
          command.Parameters.Add(command.CreateParameter("@ShipAddress", updatedOrder.ShipAddress));
          command.Parameters.Add(command.CreateParameter("@ShipCity", updatedOrder.ShipCity));
          command.Parameters.Add(command.CreateParameter("@ShipRegion", updatedOrder.ShipRegion));
          command.Parameters.Add(command.CreateParameter("@ShipPostalCode", updatedOrder.ShipPostalCode));
          command.Parameters.Add(command.CreateParameter("@ShipCountry", updatedOrder.ShipCountry));

          command.ExecuteNonQuery();
        }
      }
    }

    public void DeleteOrder(int id)
    {
      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "delete from dbo.Orders where OrderId = @id and (OrderDate is not null or ShippedDate is not null)";

          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@id", id));

          command.ExecuteNonQuery();
        }
      }
    }

    public void SetOrderDate(int id, DateTime orderDate)
    {
      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "update dbo.Orders set OrderDate = @OrderDate where OrderId = @OrderId";

          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@OrderId", id));
          command.Parameters.Add(command.CreateParameter("@OrderDate", orderDate));

          command.ExecuteNonQuery();
        }
      }
    }

    public void SetShippedDate(int id, DateTime shippedDate)
    {
      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText =
            "update dbo.Orders set ShippedDate = @ShippedDate where OrderId = @OrderId";

          command.CommandType = CommandType.Text;

          command.Parameters.Add(command.CreateParameter("@OrderId", id));
          command.Parameters.Add(command.CreateParameter("@ShippedDate", shippedDate));

          command.ExecuteNonQuery();
        }
      }
    }

    public IEnumerable<OrderHist> CustOrderHist(string customerId)
    {
      var orderHists = new List<OrderHist>();

      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText = "CustOrderHist";
          command.CommandType = CommandType.StoredProcedure;

          command.Parameters.Add(command.CreateParameter("@CustomerId", customerId));

          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              var orderHist = this.GetOrderHist(reader);

              orderHists.Add(orderHist);
            }
          }
        }
      }

      return orderHists;
    }

    public IEnumerable<OrdersDetailSp> CustOrdersDetail(int id)
    {
      var ordersDetailsSp = new List<OrdersDetailSp>();

      using (var connection = _providerFactory.CreateConnection())
      {
        connection.ConnectionString = _connectionString;
        connection.Open();

        using (var command = connection.CreateCommand())
        {
          command.CommandText = "CustOrdersDetail";
          command.CommandType = CommandType.StoredProcedure;

          command.Parameters.Add(command.CreateParameter("@OrderId", id));

          using (var reader = command.ExecuteReader())
          {
            while (reader.Read())
            {
              var ordersDetailSp = this.GetOrdersDetailSp(reader);

              ordersDetailsSp.Add(ordersDetailSp);
            }
          }
        }
      }

      return ordersDetailsSp;
    }

    private OrderHist GetOrderHist(DbDataReader reader)
    {
      var orderHist = new OrderHist
      {
        ProductName = reader.SafeGetString(0),
        Total = reader.SafeGetInt32(1)
      };

      return orderHist;
    }

    private OrdersDetailSp GetOrdersDetailSp(DbDataReader reader)
    {
      var orderDetailSp = new OrdersDetailSp
      {
        ProductName = reader.SafeGetString(0),
        UnitPrice = reader.SafeGetDecimal(1),
        Quantity = reader.SafeGetInt16(2),
        Discount = reader.SafeGetInt32(3),
        ExtendedPrice = reader.SafeGetDecimal(4)
      };

      return orderDetailSp;
    }

    private Order GetOrder(DbDataReader reader)
    {
      var order = new Order
      {
        OrderId = reader.SafeGetInt32(0),
        CustomerId = reader.SafeGetString(1),
        EmployeeId = reader.SafeGetInt32(2),
        OrderDate = reader.SafeGetDateTime(3),
        RequiredDate = reader.SafeGetDateTime(4),
        ShippedDate = reader.SafeGetDateTime(5),
        ShipVia = reader.SafeGetInt32(6),
        Freight = reader.SafeGetDecimal(7),
        ShipName = reader.SafeGetString(8),
        ShipAddress = reader.SafeGetString(9),
        ShipCity = reader.SafeGetString(10),
        ShipRegion = reader.SafeGetString(11),
        ShipPostalCode = reader.SafeGetString(12),
        ShipCountry = reader.SafeGetString(13)
      };

      return order;
    }

    private OrderDetail GetOrderDetail(DbDataReader reader)
    {
      var orderDetail = new OrderDetail
      {
        OrderId = reader.SafeGetInt32(0),
        ProductId = reader.SafeGetInt32(1),
        UnitPrice = reader.SafeGetDecimal(2),
        Quantity = reader.SafeGetInt16(3),
        Discount = reader.SafeGetFloat(4),
        ProductName = reader.SafeGetString(5)
      };

      return orderDetail;
    }
  }
}