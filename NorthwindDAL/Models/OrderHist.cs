﻿namespace NorthwindDAL.Models
{
  public class OrderHist
  {
    public string ProductName { get; set; }
    public int? Total { get; set; }
  }
}