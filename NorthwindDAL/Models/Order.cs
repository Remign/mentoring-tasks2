﻿using System;

namespace NorthwindDAL.Models
{
  public enum OrderStatus
  {
    NotSended,
    NotDelivered,
    Performed
  }

  public class Order
  {
    public Order()
    {
    }

    protected Order(Order other)
    {
      this.OrderId = other.OrderId;
      this.CustomerId = other.CustomerId;
      this.EmployeeId = other.EmployeeId;
      this.OrderDate = other.OrderDate;
      this.RequiredDate = other.RequiredDate;
      this.ShippedDate = other.ShippedDate;
      this.ShipVia = other.ShipVia;
      this.Freight = other.Freight;
      this.ShipName = other.ShipName;
      this.ShipAddress = other.ShipAddress;
      this.ShipCity = other.ShipCity;
      this.ShipRegion = other.ShipRegion;
      this.ShipPostalCode = other.ShipPostalCode;
      this.ShipCountry = other.ShipCountry;
    }

    public int? OrderId { get; set; }
    public string CustomerId { get; set; }
    public int? EmployeeId { get; set; }
    public DateTime? OrderDate { get; set; }
    public DateTime? RequiredDate { get; set; }
    public DateTime? ShippedDate { get; set; }
    public int? ShipVia { get; set; }
    public decimal? Freight { get; set; }
    public string ShipName { get; set; }
    public string ShipAddress { get; set; }
    public string ShipCity { get; set; }
    public string ShipRegion { get; set; }
    public string ShipPostalCode { get; set; }
    public string ShipCountry { get; set; }

    public OrderStatus OrderStatus
    {
      get
      {
        return this.OrderDate == null
        ? OrderStatus.NotSended
        : this.ShippedDate == null ? OrderStatus.NotDelivered : OrderStatus.Performed;
      }
    }
  }
}