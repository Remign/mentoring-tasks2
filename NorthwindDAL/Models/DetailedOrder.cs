﻿using System.Collections.Generic;

namespace NorthwindDAL.Models
{
  public class DetailedOrder : Order
  {
    public DetailedOrder()
    {
      this.OrderDetails = new List<OrderDetail>();
    }

    public DetailedOrder(Order other) : base(other)
    {
      this.OrderDetails = new List<OrderDetail>();
    }

    public List<OrderDetail> OrderDetails { get; set; } 
  }
}