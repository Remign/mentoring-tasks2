﻿namespace NorthwindDAL.Models
{
  public class OrdersDetailSp
  {
    public string ProductName { get; set; }
    public decimal? UnitPrice { get; set; }
    public short? Quantity { get; set; }
    public int? Discount { get; set; }
    public decimal? ExtendedPrice { get; set; }
  }
}