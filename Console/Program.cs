﻿using System;
using NorthwindDAL.Models;
using NorthwindDAL.Repositories;

namespace Console
{
  class Program
  {
    private const string ConnString = @"Data Source=(localdb)\Projects;Initial Catalog=Northwind;Integrated Security=True;Pooling=False;Connect Timeout=30";

    private const string ProviderName = "System.Data.SqlClient";

    static void Main(string[] args)
    {
      var repo = new OrderRepository(ProviderName, ConnString);

      var result = repo.GetOrders();

      var detailed = repo.GetDetailedOrderById(10248);

      var newOrder = new Order
      {
        CustomerId = "VINET",
        EmployeeId = 5,
        OrderDate = null,
        RequiredDate = null,
        ShippedDate = null,
        ShipVia = 3,
        Freight = (decimal)32.3800,
        ShipName = "Vins et alcools",
        ShipAddress = "Chevalier	59 rue de l'Abbaye",
        ShipCity = "alcools",
        ShipRegion = null,
        ShipPostalCode = "51100",
        ShipCountry = "France"
      };

      var newId = repo.CreateOrder(newOrder);

      newOrder.OrderId = newId;

      newOrder.ShipName = "AFAFKFK";

      repo.UpdateOrder(newOrder);

      repo.SetOrderDate(newId, DateTime.Now);
      repo.SetShippedDate(newId, DateTime.Now);

      repo.DeleteOrder(newId);

      var orderHists = repo.CustOrderHist("HUNGO");

      var orderDetailsSp = repo.CustOrdersDetail(10248);
    }
  }
}
