using EFtask.Models;

namespace EFtask.Migrations
{
  using System;
  using System.Data.Entity;
  using System.Data.Entity.Migrations;
  using System.Linq;

  internal sealed class Configuration : DbMigrationsConfiguration<EFtask.NorthwindDb>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(EFtask.NorthwindDb context)
    {
      //  This method will be called after migrating to the latest version.

      //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
      //  to avoid creating duplicate seed data. E.g.
      //
      //    context.People.AddOrUpdate(
      //      p => p.FullName,
      //      new Person { FullName = "Andrew Peters" },
      //      new Person { FullName = "Brice Lambson" },
      //      new Person { FullName = "Rowan Miller" }
      //    );
      //

      context.Categories.AddOrUpdate(p => p.CategoryID,
        new Category { CategoryName = "1"}
      );

      context.Regions.AddOrUpdate(p => p.RegionID,
        new Region { RegionDescription = "1" }
      );

      context.SaveChanges();

      var lastRegionId = context.Regions.First().RegionID;

      context.Territories.AddOrUpdate(p => p.TerritoryID,
        new Territory { TerritoryID = "qwe", TerritoryDescription = "1", RegionID = lastRegionId }
      );
    }
  }
}
