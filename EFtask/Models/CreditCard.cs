﻿using System;

namespace EFtask.Models
{
  public class CreditCard
  {
    public int Id { get; set; }

    public string CardNumber { get; set; }

    public DateTime ExpireDate { get; set; }

    public int EmployeeId { get; set; }

    public virtual Employee Employee { get; set; }
  }
}