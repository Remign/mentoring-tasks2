﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFtask.Migrations;

namespace EFtask
{
  class Program
  {
    static void Main(string[] args)
    {
      using (var db = new NorthwindDb())
      {
        var orders = db.Orders.ToList();

        //task 1
        var categoryId = 1;
        var result = db.Orders.Where(order => order.OrderDetails.Any(orderDetail => orderDetail.Product.CategoryID == categoryId)).Select(item => new
        {
          Order = item,
          OrderDetails = item.OrderDetails,
          Customer = item.Customer.CompanyName,
          CurrentCategoryProducts = item.OrderDetails.
                                    Where(itemOrderDetails => itemOrderDetails.Product.CategoryID == categoryId).
                                    Select(itemOrderDetails => itemOrderDetails.Product.ProductName)
        }).ToList();

        new InitialCreate().Down();
      }
    }
  }
}
