USE Northwind 

if not exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG = DB_NAME() and TABLE_NAME = 'CreditCard' )
  CREATE TABLE [CreditCard]
    (
      [Id] INT NOT NULL PRIMARY KEY, 
        [CardNumber] VARCHAR(16) NOT NULL, 
        [ExprireDate] DATETIME NOT NULL,
        [EmployeeID] INT NOT NULL,
        CONSTRAINT "FK_Card_Employees" FOREIGN KEY 
	      (
		      "EmployeeID"
	      ) REFERENCES "dbo"."Employees" (
		      "EmployeeID"
	      )
    )

