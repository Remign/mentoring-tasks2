USE Northwind 

if exists (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_CATALOG = DB_NAME() and TABLE_NAME = 'Region')
  EXEC sp_rename 'Region', 'Regions';  

GO

if not exists (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_CATALOG = DB_NAME() and TABLE_NAME = 'Customers' and COLUMN_NAME = 'FoundationDate')
  alter table dbo.Customers
  add FoundationDate datetime