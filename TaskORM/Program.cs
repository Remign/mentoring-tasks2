﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Common;
using TaskORM.Models;

namespace TaskORM
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      using (var db = new NorthwindContext())
      {
        foreach (var order in db.Orders)
        {
          Console.WriteLine("{0}, {1}, {2}, {3}, {4}", order.OrderId, order.CustomerId, order.EmployeeId,
            order.OrderDate,
            order.ShippedDate);
        }

        //2.1
        var products = db.Products.LoadWith(p => p.Category).LoadWith(p => p.Supplier);

        foreach (var product in products)
        {
          Console.WriteLine("{0}, {1}, {2}", product.ProductName, product.Category.CategoryName,
            product.Supplier.ContactName);
        }

        //just for test purpose
        Configuration.Linq.AllowMultipleQuery = true;
        var categories = db.Categories.LoadWith(i => i.Products);

        //2.2
        var employeeRegions = db.EmployeeTerritories.LoadWith(i => i.Employee).LoadWith(i => i.Territory).ToList()
          .Select(i => new
          {
            i.Employee.FullName,
            Region = db.Regions.FirstOrDefault(region => region.RegionId == i.Territory.RegionId).RegionDescription
          });

        var employeeRegionsGrouped = employeeRegions.GroupBy(item => new {item.FullName, item.Region});

        foreach (var employeeRegion in employeeRegionsGrouped)
        {
          Console.WriteLine("{0}, {1}", employeeRegion.Key.FullName, employeeRegion.Key.Region);
        }

        //2.3
        var employeesCountByRegion = employeeRegions.GroupBy(item => item.Region);

        foreach (var elem in employeesCountByRegion)
        {
          Console.WriteLine("{0}, {1}", elem.Key, elem.Count());
        }

        //2.4
        var employeeShippers =
          db.Orders.LoadWith(i => i.Employee)
            .LoadWith(i => i.Shipper).ToList()
            .GroupBy(item => new {item.Employee.FullName, item.Shipper.CompanyName});

        foreach (var employeeShipper in employeeShippers)
        {
          Console.WriteLine("{0}, {1}", employeeShipper.Key.FullName, employeeShipper.Key.CompanyName);
        }

        //3.1
        var newId = (int)(decimal)db.Employees.InsertWithIdentity(() => new Employee{
          FirstName = "Name",
          LastName = "LastName"});

        db.EmployeeTerritories.Insert(() => new EmployeeTerritories
        {
          EmployeeId = newId,
          TerritoryId = db.EmployeeTerritories.First().TerritoryId
        });

        //3.2

        var firstId = db.Categories.First().CategoryId;
        var lastId = db.Categories.Skip(1).First().CategoryId;
        db.Products.Where(item => item.CategoryId == firstId)
          .Set(item => item.CategoryId, lastId).Update();

        //return almost to previous state
        db.Products.Where(item => item.CategoryId == lastId)
          .Set(item => item.CategoryId, firstId).Update();

        //3.3
        var newProd = new Product {ProductName = "asdas1", CategoryId = 1, SupplierId = 1};
        InsertProduct(newProd, db);
        var newProd2 = new Product { ProductName = "asdas2", CategoryId = -1, SupplierId = -1 };
        InsertProduct(newProd2, db);

        //3.4
        ChangeProductNotShipped(1, 78, db); //check orderId = 11070
        ChangeProductNotShipped(78, 1, db);
      }
    }

    private static void InsertProduct(Product newProduct, NorthwindContext db)
    {
      var length = db.Products.Count();
      var newCategoryId = 0;
      var newSupplierId = 0;

      if (db.Categories.Any(item => item.CategoryId == newProduct.CategoryId) == false)
      {
        newCategoryId = (int)(decimal)db.Categories.InsertWithIdentity(() => new Category
        {
          CategoryName = "Category" + length
        });
      }

      if (db.Suppliers.Any(item => item.SupplierId == newProduct.SupplierId) == false)
      {
        newSupplierId = (int)(decimal)db.Suppliers.InsertWithIdentity(() => new Supplier
        {
          ContactName = "Supplier" + length,
          CompanyName = "Supplier" + length
        });
      }

      length++;

      db.Products.Insert(() => new Product
      {
        ProductName = newProduct.ProductName,
        CategoryId = newCategoryId == 0 ? newProduct.CategoryId : newCategoryId,
        SupplierId = newSupplierId == 0 ? newProduct.SupplierId : newSupplierId
      });

    }

    private static void ChangeProductNotShipped(int oldProductId, int newProductId, NorthwindContext db)
    {
      var res = db.OrderDetails.LoadWith(i => i.Order)
        .Where(i => i.Order.ShippedDate == null && i.ProductId == oldProductId)
      .Set(i => i.ProductId, newProductId)
      .Update();
    }
  }
}
