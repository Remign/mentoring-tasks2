﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Categories")]
  public class Category
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int CategoryId { get; set; }

    [Column]
    public string CategoryName { get; set; }

    [Association(ThisKey = "CategoryId", OtherKey = "CategoryId")]
    public IEnumerable<Product> Products { get; set; } 
  }
}