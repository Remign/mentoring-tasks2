﻿using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Products")]
  public class Product
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int ProductId { get; set; }

    [Column]
    public string ProductName { get; set; }

    [Column]
    public int SupplierId { get; set; }

    [Column]
    public int CategoryId { get; set; }

    [Association(ThisKey = "CategoryId", OtherKey = "CategoryId")]
    public Category Category { get; set; }

    [Association(ThisKey = "SupplierId", OtherKey = "SupplierId")]
    public Supplier Supplier { get; set; }
  }
}