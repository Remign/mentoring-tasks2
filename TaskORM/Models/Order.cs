﻿using System;
using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Orders")]
  public class Order
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int OrderId { get; set; }

    [Column]
    public string CustomerId { get; set; }

    [Column]
    public int EmployeeId { get; set; }

    [Column]
    public int ShipVia { get; set; }

    [Column]
    public DateTime OrderDate { get; set; }

    [Column]
    public DateTime ShippedDate { get; set; }

    [Association(ThisKey = "EmployeeId", OtherKey = "EmployeeId")]
    public Employee Employee { get; set; }

    [Association(ThisKey = "ShipVia", OtherKey = "ShipperId")]
    public Shipper Shipper { get; set; }

    [Association(ThisKey = "OrderId", OtherKey = "OrderId")]
    public IEnumerable<OrderDetail> OrderDetails { get; set; }
  }
}