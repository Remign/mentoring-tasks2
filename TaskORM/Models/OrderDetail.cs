﻿using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Order Details")]
  public class OrderDetail
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int OrderId { get; set; }

    [Column]
    [PrimaryKey]
    public int ProductId { get; set; }

    [Association(ThisKey = "OrderId", OtherKey = "OrderId")]
    public Order Order { get; set; }
  }
}