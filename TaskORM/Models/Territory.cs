﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Territories")]
  public class Territory
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int TerritoryId { get; set; }

    [Column]
    public string TerritoryDescription { get; set; }

    [Column]
    public int RegionId { get; set; }

    [Association(ThisKey = "TerritoryId", OtherKey = "TerritoryId")]
    public IEnumerable<EmployeeTerritories> EmployeeTerritories { get; set; }

    [Association(ThisKey = "RegionId", OtherKey = "RegionId")]
    public Region Regions { get; set; }

    //public Employee Employee { get; set; }
  }
}