﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Employees")]
  public class Employee
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int EmployeeId { get; set; }

    [Column]
    public string LastName { get; set; }

    [Column]
    public string FirstName { get; set; }

    public string FullName
    {
      get
      {
        return FirstName + " " + LastName;
      }
    }

    [Association(ThisKey = "EmployeeId", OtherKey = "EmployeeId")]
    public IEnumerable<EmployeeTerritories> EmployeeTerritories { get; set; }

    [Association(ThisKey = "EmployeeId", OtherKey = "EmployeeId")]
    public IEnumerable<Order> Orders { get; set; }

    //public Territory Territory { get; set; }
  }
}