﻿using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("EmployeeTerritories")]
  public class EmployeeTerritories
  {
    [Column]
    [PrimaryKey]
    public int EmployeeId { get; set; }
    [Column]
    [PrimaryKey]
    public int TerritoryId { get; set; }

    [Association(ThisKey = "EmployeeId", OtherKey = "EmployeeId")]
    public Employee Employee { get; set; }

    [Association(ThisKey = "TerritoryId", OtherKey = "TerritoryId")]
    public Territory Territory  { get; set; }
  }
}