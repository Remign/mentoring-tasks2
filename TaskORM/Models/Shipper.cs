﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Shippers")]
  public class Shipper
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int ShipperId { get; set; }

    [Column]
    public string CompanyName { get; set; }

    [Association(ThisKey = "ShipperId", OtherKey = "ShipVia")]
    public IEnumerable<Order> Products { get; set; }
  }
}