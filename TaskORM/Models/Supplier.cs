﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Suppliers")]
  public class Supplier
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int SupplierId { get; set; }

    [Column]
    public string ContactName { get; set; }

    [Column]
    public string CompanyName { get; set; }

    [Association(ThisKey = "SupplierId", OtherKey = "SupplierId")]
    public IEnumerable<Product> Products { get; set; }
  }
}