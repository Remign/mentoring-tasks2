﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace TaskORM.Models
{
  [Table("Region")]
  public class Region
  {
    [Column]
    [PrimaryKey]
    [Identity]
    public int RegionId { get; set; }

    [Column]
    public string RegionDescription { get; set; }

    [Association(ThisKey = "RegionId", OtherKey = "RegionId")]
    public IEnumerable<Territory> Territories { get; set; }
  }
}