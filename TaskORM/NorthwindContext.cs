﻿using LinqToDB;
using LinqToDB.Data;
using TaskORM.Models;

namespace TaskORM
{
  public class NorthwindContext : DataConnection
  {
    public NorthwindContext() : base("Northwind") { }

    public ITable<Order> Orders { get { return this.GetTable<Order>(); } }
    public ITable<Category> Categories { get { return this.GetTable<Category>(); } }
    public ITable<Product> Products { get { return this.GetTable<Product>(); } }
    public ITable<Supplier> Suppliers { get { return this.GetTable<Supplier>(); } }
    public ITable<Employee> Employees { get { return this.GetTable<Employee>(); } }
    public ITable<Territory> Territories { get { return this.GetTable<Territory>(); } }
    public ITable<EmployeeTerritories> EmployeeTerritories { get { return this.GetTable<EmployeeTerritories>(); } }
    public ITable<Region> Regions { get { return this.GetTable<Region>(); } }
    public ITable<Shipper> Shippers { get { return this.GetTable<Shipper>(); } }
    public ITable<OrderDetail> OrderDetails { get { return this.GetTable<OrderDetail>(); } }
  }
}